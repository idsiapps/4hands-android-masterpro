package com.idsiapps.a4hands2gomaster;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.databinding.DataBindingUtil;
import android.graphics.Bitmap;
import android.graphics.Typeface;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.Window;
import android.webkit.ValueCallback;
import android.webkit.WebChromeClient;
import android.webkit.WebResourceRequest;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Toast;

import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.FirebaseInstanceIdService;
import com.idsiapps.a4hands2gomaster.databinding.ActivityMainBinding;
import com.tbruyelle.rxpermissions2.RxPermissions;

import java.io.File;
import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.concurrent.Callable;

import io.reactivex.Observable;
import io.reactivex.ObservableSource;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.annotations.NonNull;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.functions.Consumer;
import io.reactivex.functions.Function;
import io.reactivex.schedulers.Schedulers;

public class WebViewDemo extends AppCompatActivity {
    private static final String TAG = "WebViewDemo";
    private final static int ERROR_NETWORK_UNAVAILABLE = 1;
    private final static int ERROR_4HANDS_UNAVAILABLE = 2;
    private final int LOCATION_ACCESS_GRANTED = 66;
    private LocationManager locationManager;
    LocationListener locationListener;
    FirebaseInstanceIdService firebase;
    private ValueCallback<Uri> mUploadMessage;
    private final static int FILECHOOSER_RESULTCODE = 1;
    private ValueCallback<Uri[]> mUMA;
    private String mCM;
    protected CompositeDisposable mCompositeDisposable = new CompositeDisposable();
    private ActivityMainBinding mBinding;
    private boolean mWebViewConfigured;
    private boolean mStartPageLoaded;
    private Location mCurrentLocation;
    private String mStartingPageUrl;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        supportRequestWindowFeature(Window.FEATURE_NO_TITLE);
        super.onCreate(savedInstanceState);
        mBinding = DataBindingUtil.setContentView(this, R.layout.activity_main);
        setTvParams();
    }

    @Override
    protected void onResume() {
        super.onResume();
        checkNetwork();
    }

    @SuppressLint("NewApi")
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (Build.VERSION.SDK_INT >= 21) {
            Uri[] results = null;
            //Check if response is positive
            if (resultCode == Activity.RESULT_OK) {
                if (requestCode == FILECHOOSER_RESULTCODE) {
                    if (null == mUMA) {
                        return;
                    }
                    if (data == null) {
                        //Capture Photo if no image available
                        if (mCM != null) {
                            results = new Uri[]{Uri.parse(mCM)};
                        }
                    } else {
                        String dataString = data.getDataString();
                        if (dataString != null) {
                            results = new Uri[]{Uri.parse(dataString)};
                        }
                    }
                }
            }
            mUMA.onReceiveValue(results);
            mUMA = null;
        } else {
            if (requestCode == FILECHOOSER_RESULTCODE) {
                if (null == mUploadMessage) return;
                Uri result = data == null || resultCode != RESULT_OK ? null : data.getData();
                mUploadMessage.onReceiveValue(result);
                mUploadMessage = null;
            }
        }
    }

    private File createImageFile() throws IOException {
        @SuppressLint("SimpleDateFormat") String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        String imageFileName = "img_" + timeStamp + "_";
        File storageDir = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES);
        return File.createTempFile(imageFileName, ".jpg", storageDir);
    }

    public void sendRegistrationToServer(String token) {
        if (mStartPageLoaded) {
            Log.d(TAG, "Sending token");
            mBinding.webView.loadUrl("javascript:getNativeToken('" + token + "');");
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String permissions[], int[] grantResults) {
        switch (requestCode) {
            case LOCATION_ACCESS_GRANTED: {
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    try {
                        locationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 0, 0, locationListener);
                    } catch (SecurityException a) {

                    }
                } else {

                }
                return;
            }
        }
    }

    private void makeUseOfNewLocation(Location loc) {
        if (mStartPageLoaded) {
            Log.d(TAG, "Sending location");
            mBinding.webView.loadUrl("javascript:getNativeGeolocation('" + String.valueOf(loc.getLatitude()) + ";" + String.valueOf(loc.getLongitude()) + "');");
        }
    }

    int keyPressCount = 2;

    // BACK KEY
    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (event.getAction() == KeyEvent.ACTION_DOWN) {
            switch (keyCode) {
                case KeyEvent.KEYCODE_BACK:
                    if (mBinding.webView.canGoBack()) {
                        mBinding.webView.goBack();
                    } else {
                        if (keyPressCount == 2) {
                            Toast.makeText(getApplicationContext(), "Нажмите ещё раз для выхода", Toast.LENGTH_SHORT).show();
                            keyPressCount--;
                        } else if (keyPressCount == 1) {
                            finish();
                        }
                    }
                    return true;
                default:
                    keyPressCount = 2;
            }

        }
        return super.onKeyDown(keyCode, event);
    }

    private void configureWebView() {
        mBinding.webView.getSettings().setJavaScriptEnabled(true);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            // chromium, enable hardware acceleration
            mBinding.webView.setLayerType(View.LAYER_TYPE_HARDWARE, null);
        } else {
            // older android version, disable hardware acceleration
            mBinding.webView.setLayerType(View.LAYER_TYPE_SOFTWARE, null);
        }


        final RxPermissions rxPermissions = new RxPermissions(this);

        mBinding.webView.setWebViewClient(new WebViewClient() {


            @Override
            public void onPageStarted(WebView view, String url, Bitmap favicon) {
                if (url.contains("tel:")) {
                    mBinding.webView.stopLoading();
                    final Intent intent = new Intent(Intent.ACTION_CALL, Uri.parse(url));
                    rxPermissions
                            .request(Manifest.permission.CALL_PHONE)
                            .subscribe(new Consumer<Boolean>() {
                                @Override
                                public void accept(@NonNull Boolean aBoolean) throws Exception {
                                    if (aBoolean)
                                        startActivity(intent);
                                    else
                                        Toast.makeText(WebViewDemo.this, "Нужно разрешение", Toast.LENGTH_LONG).show();
                                }
                            });
                } else if (url.contains("/away?page=")) {
                    mBinding.webView.stopLoading();
                    Intent inta = new Intent(Intent.ACTION_VIEW);
                    inta.setData(Uri.parse(url));
                    startActivity(inta);
                } else if (url.contains("/promo?promo=")) {
                    mBinding.webView.stopLoading();
                    Intent sendIntent = new Intent();
                    sendIntent.setAction(Intent.ACTION_SEND);
                    sendIntent.putExtra(Intent.EXTRA_TEXT, url);
                    sendIntent.setType("text/plain");
                    startActivity(sendIntent);
                } else if (url.contains("mailto:")) {
                    mBinding.webView.stopLoading();
                    Intent inta = new Intent(Intent.ACTION_VIEW);
                    inta.setData(Uri.parse(url));
                    startActivity(inta);
                } else {
                    super.onPageStarted(view, url, favicon);
                }
            }

            @Override
            public boolean shouldOverrideUrlLoading(WebView view, WebResourceRequest request) {
                return super.shouldOverrideUrlLoading(view, request);
            }

//            @Override
//            public void onReceivedSslError(WebView view, SslErrorHandler handler, SslError error){
//                handler.proceed();
//            }

            @Override
            public void onPageFinished(WebView view, String url) {
                if (url.equals(mStartingPageUrl)) {
                    mStartPageLoaded = true;
                    String refreshedToken = FirebaseInstanceId.getInstance().getToken();
                    sendRegistrationToServer(refreshedToken);
                    if (mCurrentLocation != null)
                        makeUseOfNewLocation(mCurrentLocation);
                }
                super.onPageFinished(view, url);
            }
        });

        mBinding.webView.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                return true;
            }
        });
        mBinding.webView.setLongClickable(false);

        locationManager = (LocationManager) this.getSystemService(Context.LOCATION_SERVICE);

        locationListener = new LocationListener() {
            public void onLocationChanged(Location location) {
                mCurrentLocation = location;
                makeUseOfNewLocation(location);
            }

            public void onStatusChanged(String provider, int status, Bundle extras) {
            }

            public void onProviderEnabled(String provider) {
            }

            public void onProviderDisabled(String provider) {
            }
        };
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {

            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                    LOCATION_ACCESS_GRANTED);
        } else {
            try {
                locationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 0, 0, locationListener);
            } catch (SecurityException a) {
            }
        }
        firebase = new FirebaseInstanceIdService() {
            @Override
            public void onTokenRefresh() {
                // Get updated InstanceID token.
                String refreshedToken = FirebaseInstanceId.getInstance().getToken();
                sendRegistrationToServer(refreshedToken);
            }
        };
        mBinding.webView.setWebChromeClient(new WebChromeClient() {
            // For Android < 3.0
            @SuppressWarnings("unused")
            public void openFileChooser(ValueCallback<Uri> uploadMsg) {
                mUploadMessage = uploadMsg;

                Intent i = new Intent(Intent.ACTION_GET_CONTENT);
                i.addCategory(Intent.CATEGORY_OPENABLE);
                i.setType("*/*");
                startActivityForResult(Intent.createChooser(i, getString(R.string.select_file)), FILECHOOSER_RESULTCODE);

            }

            // For Android 3.0+
            @SuppressWarnings("unused")
            public void openFileChooser(ValueCallback uploadMsg, String acceptType) {
                mUploadMessage = uploadMsg;

                Intent i = new Intent(Intent.ACTION_GET_CONTENT);

                i.addCategory(Intent.CATEGORY_OPENABLE);
                i.setType(acceptType);

                startActivityForResult(Intent.createChooser(i, getString(R.string.select_file)), FILECHOOSER_RESULTCODE);
            }

            // For Android 4.1+
            @SuppressWarnings("unused")
            public void openFileChooser(ValueCallback<Uri> uploadMsg, String acceptType, String capture) {
                mUploadMessage = uploadMsg;

                Intent i = new Intent(Intent.ACTION_GET_CONTENT);

                i.addCategory(Intent.CATEGORY_OPENABLE);
                i.setType(acceptType);

                startActivityForResult(Intent.createChooser(i, getString(R.string.select_file)), FILECHOOSER_RESULTCODE);
            }

            // For Android 5.0+
            @SuppressLint("NewApi")
            public boolean onShowFileChooser(
                    WebView webView, ValueCallback<Uri[]> filePathCallback,
                    FileChooserParams fileChooserParams) {
                if (mUMA != null) {
                    mUMA.onReceiveValue(null);
                }
                mUMA = filePathCallback;
                Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                if (takePictureIntent.resolveActivity(WebViewDemo.this.getPackageManager()) != null) {
                    File photoFile = null;
                    try {
                        photoFile = createImageFile();
                        takePictureIntent.putExtra("PhotoPath", mCM);
                    } catch (IOException ex) {
                        Log.e("mitec", "Image file creation failed", ex);
                    }
                    if (photoFile != null) {
                        mCM = "file:" + photoFile.getAbsolutePath();
                        takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(photoFile));
                    } else {
                        takePictureIntent = null;
                    }
                }
                Intent contentSelectionIntent = new Intent(Intent.ACTION_GET_CONTENT);
                contentSelectionIntent.addCategory(Intent.CATEGORY_OPENABLE);
                contentSelectionIntent.setType("*/*");
                Intent[] intentArray;
                if (takePictureIntent != null) {
                    intentArray = new Intent[]{takePictureIntent};
                } else {
                    intentArray = new Intent[0];
                }

                Intent chooserIntent = new Intent(Intent.ACTION_CHOOSER);
                chooserIntent.putExtra(Intent.EXTRA_INTENT, contentSelectionIntent);
                chooserIntent.putExtra(Intent.EXTRA_TITLE, "Image Chooser");
                chooserIntent.putExtra(Intent.EXTRA_INITIAL_INTENTS, intentArray);
                startActivityForResult(chooserIntent, FILECHOOSER_RESULTCODE);
                return true;
            }
        });
        mStartingPageUrl = getString(R.string.main_url) + "?" + System.currentTimeMillis();
        mBinding.webView.loadUrl(mStartingPageUrl);
        mWebViewConfigured = true;
    }

    private Observable<Boolean> is4HandsAlive() {
        return Observable.fromCallable(new Callable<Boolean>() {
            @Override
            public Boolean call() throws Exception {
                boolean success = false;
                try {
                    HttpURLConnection urlc = (HttpURLConnection) (new URL(getString(R.string.main_url)).openConnection());
                    urlc.setRequestProperty("User-Agent", "Test");
                    urlc.setRequestProperty("Connection", "close");
                    urlc.setConnectTimeout(1500);
                    urlc.connect();
                    success = urlc.getResponseCode() == 200;
                } catch (IOException e) {
                    e.printStackTrace();
                }
                return success;
            }
        });
    }


    private Observable<Boolean> isNetworkAvailable() {
        ConnectivityManager connectivityManager
                = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return Observable.just(activeNetworkInfo != null && activeNetworkInfo.isConnected());
    }

    private void setTvParams() {
        Typeface fieldsTypeface = Typeface.createFromAsset(getAssets(),
                "fonts" + File.separator + "MullerMedium.otf");
        mBinding.networkErrorTextView.setTypeface(fieldsTypeface);
        mBinding.networkErrorTextView.setTextColor(getResources().getColor(R.color.networkErrorText));
    }

    private void setWebViewVisibility(final int errorType) {
        boolean error = errorType != 0;
        switch (errorType) {
            case ERROR_4HANDS_UNAVAILABLE:
                mBinding.networkErrorTextView.setText(getString(R.string.server_error));
                break;
            case ERROR_NETWORK_UNAVAILABLE:
                mBinding.networkErrorTextView.setText(getString(R.string.network_error));
                break;
        }
        mBinding.networkErrorTextView.setVisibility(error ? View.VISIBLE : View.GONE);
        mBinding.refreshImageView.setVisibility(error ? View.VISIBLE : View.GONE);
        mBinding.webView.setVisibility(!error ? View.VISIBLE : View.GONE);
        mBinding.refreshImageView.setEnabled(error);
        if (!error && !mWebViewConfigured)
            configureWebView();
    }

    private void checkNetwork() {
        isNetworkAvailable()
                .flatMap(new Function<Boolean, ObservableSource<Boolean>>() {
                    @Override
                    public ObservableSource<Boolean> apply(@NonNull Boolean aBoolean) throws Exception {
                        if (aBoolean)
                            return is4HandsAlive().subscribeOn(Schedulers.io());
                        else throw new RuntimeException(String.valueOf(ERROR_NETWORK_UNAVAILABLE));
                    }
                }).observeOn(AndroidSchedulers.mainThread())
                .map(new Function<Boolean, Boolean>() {
                    @Override
                    public Boolean apply(@NonNull Boolean aBoolean) throws Exception {
                        if (aBoolean)
                            return true;
                        else throw new RuntimeException(String.valueOf(ERROR_4HANDS_UNAVAILABLE));
                    }
                })
                .subscribe(new Consumer<Boolean>() {
                    @Override
                    public void accept(@NonNull Boolean aBoolean) throws Exception {
                        if (aBoolean)
                            setWebViewVisibility(0);
                    }
                }, new Consumer<Throwable>() {
                    @Override
                    public void accept(@NonNull Throwable throwable) throws Exception {
                        if (throwable instanceof RuntimeException) {
                            if (throwable.getMessage().equals(String.valueOf(ERROR_NETWORK_UNAVAILABLE))) {
                                setWebViewVisibility(ERROR_NETWORK_UNAVAILABLE);
                            } else if (throwable.getMessage().equals(String.valueOf(ERROR_4HANDS_UNAVAILABLE))) {
                                setWebViewVisibility(ERROR_4HANDS_UNAVAILABLE);
                            }
                        } else if (throwable instanceof IOException) {
                            setWebViewVisibility(ERROR_4HANDS_UNAVAILABLE);
                        }
                    }
                });
    }

    public void refreshClicked(View view) {
        mBinding.refreshImageView.setEnabled(false);
        checkNetwork();
    }
}
